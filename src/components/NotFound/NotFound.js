import React from 'react';

import './NotFound.css';

const NotFound = () => (
  <div className="NotFound">
    <h1>Page not found</h1>
    <p>We are sorry but the page you are looking for does not exist.</p>
  </div>
);

export default NotFound;
