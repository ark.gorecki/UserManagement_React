import React, { Component } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { addUser } from '../users.actions';

import Form from '../Form/Form';

class AddUser extends Component {
  handleAdd = user => {
    this.props.addUser(user);
  };

  render() {
    return (
      <div className="container mt-5">
        <div className="row">
          <div className="col-12 col-md-8 offset-md-2 col-lg-6 offset-lg-3 mt-3">
            <div className="jumbotron text-center w-100">
              <Form submit={this.handleAdd} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const dispatchState = (dispatch) => {
  return {
    addUser: user => dispatch(addUser(user)),
  };
};

export default connect(null, dispatchState)(AddUser);
