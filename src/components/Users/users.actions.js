import axios from 'axios';
import Api from '../../services/Api';

const namespace = 'USERS';
export const GET_USERS = `GET_USERS_${namespace}`;
export const GET_USERS_FAIL = `GET_USERS_FAIL_${namespace}`;
export const ADD_USER = `ADD_USER_${namespace}`;
export const EDIT_USER = `EDIT_USER_${namespace}`;
export const DELETE_USER = `DELETE_USER_${namespace}`;

export const getUsers = () => (dispatch) => {
  return Api.get('users')
    .then((res) => {
      dispatch({ type: GET_USERS, users: res.data })
    })
    .catch((error) => {
      dispatch({ type: GET_USERS_FAIL, error });
    });
};

export const addUser = user => (dispatch) => {
  return Api.post('users', user)
    .then((res) => {
      const userArr = [res.data];
      dispatch({ type: ADD_USER, user: userArr });
    });
};

export const deleteUser = (id) => {
  return {
    type: DELETE_USER,
    id,
  };
};

export const editUser = (id, user) => {
  return {
    type: EDIT_USER,
    id,
    user,
  };
};
