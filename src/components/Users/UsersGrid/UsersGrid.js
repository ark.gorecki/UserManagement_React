import React, { Component } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { getUsers } from '../users.actions';

import UserCard from '../UserCard/UserCard';
import Spinner from '../../UI/Spinner/Spinner';
import AddNewBtn from '../../UI/AddNewButton/AddNewButton';

class UsersGrid extends Component {
  componentDidMount() {
    this.props.getUsers();
  }

  render() {
    let userCards = this.props.error ? <h3>Users can't be loaded</h3> : <Spinner />;
    if (this.props.users.length > 0) {
      userCards = this.props.users.map((user, i) => <UserCard user={user} key={i} />);
      userCards = [...userCards, <AddNewBtn key={this.props.users.length + 1} />];
    }

    return (
      <div>
        <div className="container">
          <div className="row">{userCards}</div>
        </div>
      </div>
    );
  }
}

const mapState = state => ({
  users: state.users.users,
  error: state.users.error,
});

const dispatchState = dispatch => ({
  getUsers: () => dispatch(getUsers()),
});

export default connect(mapState, dispatchState)(UsersGrid);
