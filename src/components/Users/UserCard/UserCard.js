import React from 'react';
import { Link } from 'react-router-dom';

const userCard = props => (
  <div className="col-12 col-md-6 col-lg-4 text-center mt-3">
    <div className="card">
      <div className="card-body">
        <h5 className="card-title" style={{ fontWeight: 'bold' }}>
          {props.user.name}
        </h5>
        <h6 className="card-subtitle mb-2 text-muted">{props.user.username}</h6>
        <p className="card-text">
          <small>Email</small>: {props.user.email}
          <br />
          <small>City</small>: {props.user.address.city}
        </p>
        <Link to={'/user/' + props.user.id} className="btn btn-warning">
          Show User
        </Link>
      </div>
    </div>
  </div>
);

export default userCard;
