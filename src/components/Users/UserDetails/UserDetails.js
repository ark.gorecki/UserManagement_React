import React, { Component } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import {
  editUser,
  deleteUser,
} from '../users.actions';
import Api from '../../../services/Api';
import config from '../../../config/config.json';

import Spinner from '../../UI/Spinner/Spinner';
import ShowUser from './ShowUser/ShowUser';
import Form from '../Form/Form';

class UsersDetails extends Component {
  state = {
    id: this.props.match.params.id,
    user: null,
    loading: false,
    error: null,
    edit: false,
  };

  componentDidMount() {
    this.getUser();
  }

  getUser = () => {
    this.setState({ loading: true });
    Api.get(`users/${this.state.id}`).then((res) => {
      this.setState({
        user: res.data,
        loading: false,
      })
    })
      .catch(error => this.setState({ error }));
  };

  changeToEdit = () => {
    this.setState({
      edit: !this.state.edit,
    });
  };

  handleEdit = (user) => {
    const url = this.state.url;
    this.setState({
      loading: true,
      edit: false,
    });
    axios
      .patch(url, {
        name: user.name,
        username: user.username,
        email: user.email,
        address: {
          city: user.address.city,
        },
        company: {
          name: user.company.name,
        }
      })
      .then((res) => {
        console.log(res);
        this.setState({
          user: res.data,
          loading: false,
        });
        this.props.editUser(Number(this.state.id), user);
      })
      .catch(error =>
        this.setState({
          error,
          loading: false,
        }));

    this.props.editUser((this.state.id) * 1, user);
  };

  handleDelete = () => {
    const url = `${config.API_URL}users/${this.state.id}`;
    axios
      .delete(url)
      .then((res) => {
        console.log(res);
        this.props.history.push('/');
        this.props.deleteUser(Number(this.state.id));
      })
      .catch(error => this.setState({ error }));
  };

  render() {
    let user = this.state.error ? <h3>User can't be loaded</h3> : <Spinner />;
    if (this.state.user) {
      user = (
        <ShowUser user={this.state.user} edit={this.changeToEdit} delete={this.handleDelete} />
      );
    }

    const editUser = this.state.edit ? (
      <Form user={this.state.user} submit={this.handleEdit} />
    ) : null;

    const display = !this.state.edit ? user : editUser;

    return (
      <div className="container mt-5">
        <div className="row">
          <div className="col-12 col-md-8 offset-md-2 col-lg-6 offset-lg-3 mt-3">
            <div className="jumbotron text-center w-100">{display}</div>
          </div>
        </div>
      </div>
    );
  }
}

const dispatchState = (dispatch) => {
  return {
    editUser: (id, user) => dispatch(editUser(id, user)),
    deleteUser: id => dispatch(deleteUser(id)),
  };
};

export default connect(null, dispatchState)(UsersDetails);
