import React from 'react';

import Aux from '../../../../hoc/Auxilary/Auxilary';

const showUser = props => (
  <Aux>
    <h1 className="display-4">{props.user.name}</h1>
    <h2>{props.user.username}</h2>
    <p className="lead mt-5">
      <span>Email: {props.user.email}</span>
    </p>
    <p className="lead">
      <span>City: {props.user.address.city}</span>
    </p>
    <p className="lead">Company: {props.user.company.name}</p>
    <hr className="my-4" />
    <p className="lead">
      <button className="btn btn-success btn-md mr-4" onClick={props.edit}>
        Edit
      </button>
      <button className="btn btn-danger btn-md" onClick={props.delete}>
        Delete
      </button>
    </p>
  </Aux>
);

export default showUser;
