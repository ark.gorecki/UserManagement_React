import React, { Component } from 'react';

class Form extends Component {
  state = {
    user: this.props.user ? this.props.user : ''
  };

  handleSubmit = e => {
    e.preventDefault();

    const user = {
      name: this.refs.name.value,
      username: this.refs.username.value,
      email: this.refs.email.value,
      address: {
        city: this.refs.city.value
      },
      company: {
        name: this.refs.company.value
      }
    };

    this.props.submit(user);
  };

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <div className="form-group">
          <label>
            <strong>Name:</strong>
          </label>
          <input
            type="text"
            className="form-control text-center"
            id="name"
            ref="name"
            defaultValue={this.state.user ? this.state.user.name : ''}
            required
          />
        </div>
        <div className="form-group">
          <label>
            <strong>Username:</strong>
          </label>
          <input
            type="text"
            className="form-control text-center"
            id="username"
            ref="username"
            defaultValue={this.state.user ? this.state.user.username : ''}
            required
          />
        </div>
        <div className="form-group">
          <label>
            <strong>Email address</strong>
          </label>
          <input
            type="email"
            className="form-control text-center"
            id="email"
            ref="email"
            defaultValue={this.state.user ? this.state.user.email : ''}
            required
          />
        </div>
        <div className="form-group">
          <label>
            <strong>City:</strong>
          </label>
          <input
            type="text"
            className="form-control text-center"
            id="city"
            ref="city"
            defaultValue={
              this.state.user.address ? this.state.user.address.city : ''
            }
            required
          />
        </div>
        <div className="form-group">
          <label>
            <strong>Company:</strong>
          </label>
          <input
            type="text"
            className="form-control text-center"
            id="company"
            ref="company"
            defaultValue={this.state.user ? this.state.user.company.name : ''}
            required
          />
        </div>
        <button type="submit" className="btn btn-success">
          Send
        </button>
      </form>
    );
  }
}

export default Form;
