import { GET_USERS, GET_USERS_FAIL, ADD_USER, DELETE_USER } from './users.actions';

const initialState = {
  users: [],
  error: null,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
  case GET_USERS:
    return Object.assign({}, state, {
      users: action.users,
    });
  case GET_USERS_FAIL:
    return Object.assign({}, state, {
      error: action.error,
    });
  case ADD_USER:
    return Object.assign({}, state, {
      users: state.users.concat(action.user),
    });
  case DELETE_USER:
    return Object.assign({}, state, {
      users: state.users.filter(el => el.id !== action.id),
    });
  default:
    return state;
  }
};

export default reducer;
