import React from 'react';
import { Link } from 'react-router-dom';

import './AddNewButton.css';

const addNewBtn = props => (
  <div className="col-12 col-md-6 col-lg-4 text-center mt-3">
    <div className="card" style={{ height: '200px' }}>
      <div className="card-body">
        <Link to="/user/new" className="btn btn-warning btn-lg button">
          Add New
        </Link>
      </div>
    </div>
  </div>
);

export default addNewBtn;
