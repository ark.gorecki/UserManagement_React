import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import './App.css';

import Layout from '../hoc/Layout';
import UsersGrid from '../components/Users/UsersGrid/UsersGrid';
import UserDetails from '../components/Users/UserDetails/UserDetails';
import AddUser from '../components/Users/AddUser/AddUser';
import NotFound from '../components/NotFound/NotFound';

class App extends Component {
  render() {
    return (
      <Layout>
        <main>
          <Switch>
            <Route path="/user/new" exact component={AddUser} />
            <Route path="/user/:id" exact component={UserDetails} />
            <Route path="/" exact component={UsersGrid} />
            <Route path="*" component={NotFound} />
          </Switch>
        </main>
      </Layout>
    );
  }
}

export default App;
