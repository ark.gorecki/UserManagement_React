import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

import './Layout.css';

class Layout extends Component {
  render() {
    return (
      <div className="Layout">
        <NavLink to="/">
          <h1 className="Title">Users List</h1>
        </NavLink>
        {this.props.children}
      </div>
    );
  }
}

export default Layout;
