import axios from 'axios';
import config from '../config/config.json';

class Api {
  get(route, params, routeParams, url) {
    return this.xhr(route, params, 'GET', url);
  }

  getWithRouteParams(route, routeParams) {
    const keys = Object.keys(routeParams);
    const values = Object.values(routeParams);
    const params = keys.map((el, i) => `${el}=${values[i]}`);
    return `${route}?${params.splice(',')}`;
  }

  post(route, params, routeParams, url) {
    const newRoute = route;
    return this.xhr(newRoute, params, 'POST', url);
  }

  xhr(route, params, verb, url = config.API_URL) {
    const uri = `${url}${route}`;
    return axios({
      method: verb,
      url: uri,
      headers: {
        'Content-Type': 'application/json;charset=UTF-8',
        Accept: 'application/json, text/plain',
      },
      data: params,
    });
  }
}

export default new Api();
