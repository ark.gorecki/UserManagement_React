import { combineReducers } from 'redux';
import users from '../components/Users/users.reducers';

export default combineReducers({
  users,
});